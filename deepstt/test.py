import os
from django.test import TestCase
import requests
import json


class SpeechToTextTest(TestCase):

    def setUp(self):
        self.audioTestFile = "{base_path}/test_audio/aditi_en_in.wav".format(
            base_path=os.path.abspath(os.path.dirname(__file__)))

    def testSTT(self):
        files = {'audio': open(self.audioTestFile, 'rb')}
        try:
            resp = requests.post('http://127.0.0.1:8000/deepstt/to_text/', files=files)
            assert json.loads(resp.text)['success'] == True
        except requests.exceptions.ConnectionError as e:
            assert False, "Run server on localhost before running this test"
