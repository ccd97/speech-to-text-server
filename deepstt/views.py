import os

from deepspeech.model import Model
import scipy.io.wavfile as wav
from django.core.files.temp import NamedTemporaryFile
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

DS_GRAPH = "{base_path}/deepspeech_models/output_graph.pb".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))
DS_LMBIN = "{base_path}/deepspeech_models/lm.binary".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))
DS_TRIE = "{base_path}/deepspeech_models/trie".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))
DS_ALPBET = "{base_path}/deepspeech_models/alphabet.txt".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))


# Constants - Don't change for pretrained model1
BEAM_WIDTH = 500
N_FEATURES = 26
N_CONTEXT = 9
LM_WEIGHT = 1.75
WORD_COUNT_WEIGHT = 1.00
VALID_WORD_COUNT_WEIGHT = 1.00


@csrf_exempt
def speech_to_text(request):
    data = {"success": False}

    if request.method == "POST":
        tmp_f = NamedTemporaryFile(mode="wb+", suffix='.wav')

        if request.FILES.get("audio", None) is not None:
            audio_request = request.FILES["audio"]
            for chunk in audio_request.chunks():
                tmp_f.write(chunk)
            tmp_f_another_instance = open(tmp_f.name, 'rb')
            fs, audio = wav.read(tmp_f_another_instance)
            assert fs == 16000
            stt_text = run_stt(audio, fs)
            tmp_f_another_instance.close()

            if stt_text:
                data['success'] = True
                data['stt_text'] = stt_text

    return JsonResponse(data)


def run_stt(audio, fs):
    if not (os.path.isfile(DS_GRAPH) and os.path.isfile(DS_ALPBET)
            and os.path.isfile(DS_TRIE) and os.path.isfile(DS_TRIE)):
        print("===========================")
        print("PRETRAINED MODELS NOT FOUND")
        print("===========================")
        print("Download pretrained model from https://github.com/mozilla/DeepSpeech/releases")
        print("Unzip it and place it inside deepspeech_models folder")
        print("===========================")
        return None

    ds_model = Model(DS_GRAPH, N_FEATURES, N_CONTEXT, DS_ALPBET, BEAM_WIDTH)
    ds_model.enableDecoderWithLM(DS_ALPBET, DS_LMBIN, DS_TRIE, LM_WEIGHT,
                                 WORD_COUNT_WEIGHT, VALID_WORD_COUNT_WEIGHT)
    return ds_model.stt(audio, fs)
